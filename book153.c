/**
 * 程序名：book153.c，此程序用于演示#ifdef条件编译。
 * 日期：2020-09-06
 */
#include <stdio.h>

#define LINUX

int main()
{
    #ifdef LINUX
        printf("这是Linux操作系统。\n");
    #else
        printf("未知的操作系统。\n");
    #endif

    return 0;
}
