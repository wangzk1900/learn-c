/**
 * 程序名；book130.c，此程序演示时间操作的mktime库函数。
 * 日期：2020-09-06
 */
#include <stdio.h>
#include <time.h>
#include <string.h>

int main(int argc, char *argv[])
{
    //2020-09-06 09:38:35 整数表示是：1599356315
    struct tm sttm;
    memset(&sttm, 0, sizeof(sttm));

    sttm.tm_year = 2020-1900;
    sttm.tm_mon = 9-1;
    sttm.tm_mday = 6;
    sttm.tm_hour = 9;
    sttm.tm_min = 38;
    sttm.tm_sec = 35;
    sttm.tm_isdst = 0;

    printf("2020-09-06 09:38:35 is %lu\n", mktime(&sttm));

    return 0;
}
