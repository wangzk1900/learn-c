/**
 * 程序名：book150.c，此程序演示带参数的宏。
 * 日期：2020-09-06
 */
#include <stdio.h>

#define MAX(x,y) ((x)>(y) ? (x) : (y))

int main()
{
    printf("MAX is %d\n", MAX(34, 59));

    return 0;
}
