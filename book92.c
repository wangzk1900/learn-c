/**
 * 程序名：book92.c，此程序演示结构体的访问（使用）
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50]; //姓名
    int  age; //年龄
    int  height; //身高
    char sc[30]; //身材
    char yz[30]; //颜值
};

int main()
{
    struct st_girl queen;

    //初始化结构体变量
    memset(&queen, 0, sizeof(struct st_girl));

    //对结构体成员赋值
    strcpy(queen.name, "武则天");
    queen.age = 28;
    queen.height = 168;
    strcpy(queen.sc, "丰满");
    strcpy(queen.yz, "美丽");

    printf("姓名：%s\n", queen.name);
    printf("年龄：%d\n", queen.age);
    printf("身高：%d\n", queen.height);
    printf("身材：%s\n", queen.sc);
    printf("颜值：%s\n", queen.yz);
}
