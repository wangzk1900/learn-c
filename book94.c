/**
 * 程序名：book94.c，此程序演示采用memcpy函数复制结构体。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50];
    int  age;
};

int main()
{
    struct st_girl girl1, girl2;

    //对girl1的成员变量赋值
    strcpy(girl1.name, "西施");
    girl1.age = 18;

    //把girl1中的内容复制到girl2中
    memcpy(&girl2, &girl1, sizeof(struct st_girl));

    printf("girl1.name = %s, girl1.age = %d\n", girl1.name, girl1.age);
    printf("girl2.name = %s, girl2.age = %d\n", girl2.name, girl2.age);

    return 0;
}
