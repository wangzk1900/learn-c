/**
 * 程序名：book117.c，此程序用于演示从文件中读取二进制数据。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50];
    int  age;
    int  height;
};

int main()
{
    struct st_girl stgirl;
    FILE *fp = 0;

    //以只读的方式打开文件/tmp/test1.dat
    if( (fp = fopen("/tmp/test1.dat", "rb")) == 0)
    {
        printf("fopen(/tmp/test1.dat) failed.\n");
        return -1;
    }

    while(1)
    {
        if( fread(&stgirl, 1, sizeof(struct st_girl), fp) == 0 )
            break;
        printf("name = %s, age = %d, height = %d\n",\
                stgirl.name, stgirl.age, stgirl.height);

    }

    //关闭文件
    fclose(fp);

    return 0;
}
