/**
 * 程序名：book78.c，此程序测试浮点数据的科学计数法。
 * 日期：2020-09-04
 */
#include <stdio.h>

int main()
{
    double dd;

    dd = 123000000;
    printf("dd is %.2e\n", dd);

    dd = -123000000;
    printf("dd is %.2e\n", dd);

    dd = 0.0000000123;
    printf("dd is %.2e\n", dd);

    dd = -0.0000000123;
    printf("dd is %.2e\n", dd);

    return 0;
}
