/**
 * 程序名：book111.c，此程序用于演示向文件中写入文本数据。
 * 日期：2020-09-05
 */
#include <stdio.h>

int main()
{
    int i = 0;
    FILE *fp = 0;

    //以只写的方式打开文件/tmp/test1.txt
    if( (fp = fopen("/tmp/test1.txt", "w")) == 0)
    {
        printf("fopen(/tmp/test1.txt) failed.\n");
        return -1;
    }

    for(i = 0; i < 3; i++)
    {
        fprintf(fp, "这是第%d个出场的超女。\n", i+1);
    }

    //关闭文件
    fclose(fp);

    return 0;
}
