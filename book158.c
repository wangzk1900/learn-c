/**
 * 程序名：book158.c，此程序演示strerror函数的使用。
 * 日期：2020-09-06
 */
#include <stdio.h>
#include <string.h>

int main()
{
    int errorno;

    for(errorno = 0; errorno < 132; errorno++)
    {
        printf("%d:%s\n", errorno, strerror(errorno));
    }

    return 0;
}
