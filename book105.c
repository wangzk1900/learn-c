/**
 * 程序名：book105.c，此程序用于演示当前程序运行环境的参数envp。
 * 日期：2020-09-05
 */
#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
    int i = 0;

    while(envp[i] != 0)
    {
        printf("%s\n", envp[i]);
        i++;
    }
}
