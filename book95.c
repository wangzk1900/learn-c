/**
 * 程序名：book95.c，此程序演示结构体作为函数的参数。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50];
    int  age;
};

//对结构体赋值的函数
void setvalue(struct st_girl *pst);

int main()
{
    struct st_girl queen;

    //初始化结构体变量
    memset(&queen, 0, sizeof(struct st_girl));
    //调用函数，传入结构体的地址
    setvalue(&queen);

    printf("姓名：%s，年龄：%d\n", queen.name, queen.age);

    return 0;
}

void setvalue(struct st_girl *pst)
{
    //对结构体成员赋值
    strcpy(pst->name, "武则天");
    pst->age = 28;
}
