/**
 * 程序名：book149.c，此程序演示不带参数的宏。
 * 日期：2020-09-06
 */
#include <stdio.h>

#define PI 3.141592

int main()
{
    printf("PI is %lf\n", PI);

    return 0;
}
