/**
 * 程序名：book108.c，此程序用于演示文件的打开和关闭。
 * 日期：2020-09-05
 */
#include <stdio.h>

int main()
{
    FILE *fp = 0;

    //以只读的方式打开文件/root/book.c
    if( (fp = fopen("/root/book.c", "r")) == 0)
    {
        printf("打开文件/root/book.c失败。\n");
        return -1;
    }

    /*
    fp = fopen("/root/book.c", "r");
    if(fp == 0)
    {
        printf("打开文件/root/book.c失败。\n");
        return -1;
    }
    */

    printf("fp = %p\n", (fp = fopen("/root/book.c", "r")));
    printf("fp = %p\n", fp);

    //关闭文件
    fclose(fp);

    return 0;
}
        
