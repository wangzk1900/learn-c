/**
 * 程序名：book98.c，此程序演示格式化输出sprintf和snprintf函数。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

int main()
{
    char str[128];

    //格式化输出到str中
    sprintf(str, "%d, %c, %f, %s", 10, 'A', 25.7, "一共输入了三个数。");
    printf("%s\n", str);

    //格式化输出到str中，只截取前7个字符
    snprintf(str, 8, "%d, %c, %f, %s", 10, 'A', 25.7, "一共输入了三个数。");
    printf("%s\n", str);

    return 0;
}
