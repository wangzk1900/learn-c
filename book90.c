/**
 * 程序名：book90.c，此程序用于演示C语言的结构体占用内存的情况。
 * 日期：2020-09-05
 */
#include <stdio.h>
#pragma pack(1)

struct st_girl
{
    char name[50]; //姓名
    int  age; //年龄
    int  height; //身高
    char sc[30]; //身材
    char yz[30]; //颜值
};

int main()
{
    struct st_girl queen;
    printf("sizeof(struct st_girl): %d\n", sizeof(struct st_girl));
    printf("sizeof(queen): %d\n", sizeof(queen));
    printf("sizeof(name+age+height+sc+yz): %d\n",50+sizeof(int)+sizeof(int)+30+30 );
}
