/**
 * 程序名：book124.c，此程序用于演示读取目录及其子目录下全部的文件信息。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <dirent.h>

//列出目录及子目录下的文件
int ReadDir(const char *strpathname);

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("请指定目录名。\n");
        return -1;
    }
    
    ReadDir(argv[1]);
}

//列出目录及子目录下的文件
int ReadDir(const char *strpathname)
{
    DIR *dir;
    char strchdpath[256]; //子目录全路径

    if( (dir = opendir(strpathname)) == 0)
        return -1;

    //用于存放从目录读取到的文件和目录信息
    struct dirent *stdinfo;

    while(1)
    {
        //读取一条记录
        if( (stdinfo = readdir(dir)) == 0)
            break;

        //过滤掉以 . 开始的文件
        if( strncmp(stdinfo->d_name, ".", 1) == 0)
            continue;

        //若是文件，则显示到屏幕
        if(stdinfo->d_type == 8)
        {
            printf("name = %s/%s\n", strpathname, stdinfo->d_name);
        }

        //若是目录，则调用一次ReadDir
        if(stdinfo->d_type == 4)
        {
            sprintf(strchdpath, "%s/%s", strpathname, stdinfo->d_name);
            ReadDir(strchdpath);
        }
    }

    //关闭目录指针
    closedir(dir);
}
