/**
 * 程序名：book115.c，此程序用于演示向文件中写入二进制数据。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50];
    int  age;
    int  height;
};

int main()
{
    struct st_girl stgirl;
    FILE *fp = 0;

    //以只写的方式打开文件/tmp/test1.dat
    if( (fp = fopen("/tmp/test1.dat", "w")) == 0)
    {
        printf("fopen(/tmp/test1.dat) failed.\n");
        return -1;
    }

    strcpy(stgirl.name, "西施");
    stgirl.age = 18;
    stgirl.height = 170;
    fwrite(&stgirl, 1, sizeof(stgirl), fp);

    strcpy(stgirl.name, "貂蝉");
    stgirl.age = 16;
    stgirl.height = 165;
    fwrite(&stgirl, 1, sizeof(stgirl), fp);

    //关闭文件
    fclose(fp);

    return 0;
}
