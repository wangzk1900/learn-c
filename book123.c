/**
 * 程序名：book123.c，此程序用于演示读取目录下的文件名信息。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <dirent.h>

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("请指定目录名。\n");
        return -1;
    }

    DIR *dir;

    //打开目录
    if( (dir = opendir(argv[1])) == 0 )
        return -1;

    //用于存放从目录中读取到的文件和目录信息
    struct dirent *stdinfo;

    while(1)
    {
        //读取一条记录并显示到屏幕
        if( (stdinfo = readdir(dir)) == 0)
            break;

        printf("name=%s, type=%d\n", stdinfo->d_name, stdinfo->d_type);
    }

    //关闭目录指针
    closedir(dir);

    return 0;
}
