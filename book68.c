/**
 * 程序名：book68.c，此程序演示字符与整数的关系
 * 日期：2020-09-04
 *
 */
#include <stdio.h>

int main()
{
    char a = 'E';
    char b = 70;
    int c = 71;
    int d = 'H';

    printf("a=%c, a=%d\n", a, a);
    printf("b=%c, b=%d\n", b, b);
    printf("c=%c, c=%d\n", c, c);
    printf("d=%c, d=%d\n", d, d);

    return 0;
}
