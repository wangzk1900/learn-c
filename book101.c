/**
 * 程序名：book101.c，此程序演示main函数的参数。
 * 日期：2020-09-05
 */
#include <stdio.h>

int main(int argc, char *argv[])
{
    int i = 0;

    //显示参数的个数
    printf("argc is %d\n", argc);

    //列出全部参数
    for(i = 0; i < argc; i++)
    {
        printf("argv[%d] is %s\n", i, argv[i]);
    }

    return 0;
}
