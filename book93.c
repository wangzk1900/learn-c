/**
 * 程序名：book93.c，此程序演示结构体的指针。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

struct st_girl
{
    char name[50];
    int  age;
};

int main()
{
    struct st_girl *pst, queen;

    //初始化结构体变量
    memset(&queen, 0, sizeof(struct st_girl));

    pst = &queen;

    //对结构体成员赋值
    strcpy(pst->name, "武则天");
    pst->age=28;

    printf("姓名：%s，年龄：%d\n", queen.name, queen.age);
    printf("姓名：%s，年龄：%d\n", pst->name, pst->age);
    printf("姓名：%s，年龄：%d\n", (*pst).name, (*pst).age);

    return 0;
}
