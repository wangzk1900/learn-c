/**
 * 程序名：book113.c，此程序用于演示从文本文件中读取数据。
 * 日期：2020-09-05
 */
#include <stdio.h>
#include <string.h>

int main()
{
    FILE *fp = 0;
    char strbuf[30];

    //以只读的方式打开文件/tmp/test1.txt
    if( (fp = fopen("/tmp/test1.txt", "r")) == 0)
    {
        printf("fopen(/tmp/test1.txt) failed.\n");
        return -1;
    }

    //逐行读取文件的内容，输出到屏幕
    while(1)
    {
        memset(strbuf, 0, sizeof(strbuf));
        if(fgets(strbuf, 301, fp) == 0)
            break;
        printf("%s", strbuf);
    }

    //关闭文件
    fclose(fp);

    return 0;
}
